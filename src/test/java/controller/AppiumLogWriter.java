package controller;


import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by devqa on 16/02/2017.
 */
public class AppiumLogWriter {
    private FileOutputStream fileOutputStream;
    private String appiumLogFile = "AppiumLog.txt";
    private FileWriter fileWriter;

/*    public void createFile(){
        try{
            fileOutputStream = new FileOutputStream(appiumLogFile);
        }
        catch (FileNotFoundException e1){
            System.out.println("Create file error");
        }

    }*/

/*    public void deleteFile(){
        File file = new File(appiumLogFile);
        boolean deleted = file.delete();
        if(!deleted){
            System.out.println("File was not deleted");
        }
    }*/

    public void emptyFile(){

        try{
            fileWriter = new FileWriter(appiumLogFile);
            fileWriter.write("");
            fileWriter.close();
        }
        catch (IOException e){
            System.out.println("Could not empty file");
        }
    }

    public void writeLineToFile(String line){
        try{
            fileWriter = new FileWriter(appiumLogFile,true);
            fileWriter.append(line);
            fileWriter.append("\n");
            fileWriter.close();
        }
        catch (IOException e2){
            System.out.println("Write error");
        }
    }

}
