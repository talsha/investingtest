package controller;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TestController {


    public void initTests(String address, String port, String udid, String apkPath, String buttonName, String systemPort){
        try {

            DesiredCapabilities capabilities = new DesiredCapabilities();
            //mandatory capabilities
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
            capabilities.setCapability(MobileCapabilityType.PLATFORM, MobilePlatform.ANDROID);
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.1");
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, udid);
            capabilities.setCapability(MobileCapabilityType.UDID, udid);
            capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.bublikk.appiumtestapp.MainActivity");
            capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.bublikk.appiumtestapp");
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
            capabilities.setCapability(MobileCapabilityType.APP, apkPath);
            capabilities.setCapability("systemPort", systemPort);
//            capabilities.setCapability("--session-override",true);

//            capabilities.setCapability("autoWebview", false);
//            capabilities.setCapability("enablePerformanceLogging", true);
//            capabilities.setCapability("nativeWebTap", true); //new

            AndroidDriver driver= new AndroidDriver(new URL("http://"+address+":" + port + "/wd/hub/"), capabilities);

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            System.out.println("DIMA: try to find agreeBtn");
//            PageFactory.initElements(new AppiumFieldDecorator(driver), this);
//            WebDriverWait driverWait = new WebDriverWait(driver, 20);
//            driverWait.until(ExpectedConditions.elementToBeClickable(By.id("com.bublikk.appiumtestapp:id/btn")));

            WebElement agreeBtn = null;
            if(port.equals("8723")){
                agreeBtn=driver.findElementById("com.bublikk.appiumtestapp:id/" + buttonName);
            }
            else{
                agreeBtn=driver.findElementById("com.bublikk.appiumtestapp:id/btn1");
            }

            if (agreeBtn!=null) {
                agreeBtn.click();
                System.out.println("DIMA: agreeBtn clicked   udid= "+driver.getCapabilities().getCapability("udid").toString());
                System.setOut(System.out);
            }else{
                System.out.println("DIMA: agreeBtn NOT clicked");
                System.setOut(System.out);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
