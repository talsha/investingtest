package controller;


/**
 * Created by devqa on 16/02/2017.
 */
public interface AppiumServerListener {
    void executeCommand(boolean connected, String ip, String port, String udid, String systemPort);
}
