package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class SeleniumHubManager implements Runnable {
    private SeleniumHubListener mListener;

    public SeleniumHubManager(SeleniumHubListener mListener) {
        this.mListener = mListener;
    }

    public interface SeleniumHubListener{
        public void onSeleniumHubConnected(boolean state);
    }
    @Override
    public void run() {
        startSeleniumHub();
    }
    public void startSeleniumHub(){
//        executeCommandInWindows("java -jar libs/selenium-server-standalone-3.8.1.jar -role hub -hubConfig json/grid.json", mListener);
        executeCommandInWindows("java -jar libs/selenium-server-standalone-3.8.1.jar -role hub", mListener);

    }
    public void executeCommandInWindows(String aCommand , SeleniumHubListener listener) {
        File currDir = new File(System.getProperty("user.dir"));
        String line;
        try {
            ProcessBuilder probuilder = new ProcessBuilder("CMD", "/C", aCommand);
            probuilder.directory(currDir);
            Process process = probuilder.start();

            BufferedReader inputStream
                    = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errorStream
                    = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            // reading output of the command
            int inputLine = 0;
            if (listener!=null ) {
                listener.onSeleniumHubConnected(true);
            }


        } catch (Exception e) {
            System.err.println("Exception occured: \n");
            System.err.println(e.getMessage());
            if (listener!=null ) {
                listener.onSeleniumHubConnected(true);
            }
        }
    }
}
