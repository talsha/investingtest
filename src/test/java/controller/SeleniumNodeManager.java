package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class SeleniumNodeManager implements Runnable {
    SeleniumNodeListener mListener;
    String mPort;
    String mUdid;

    public SeleniumNodeManager(SeleniumNodeListener mListener, String port, String udid) {
        this.mListener = mListener;
        this.mPort = port;
        this.mUdid = udid;
    }

    public interface SeleniumNodeListener{
        public void onSeleniumNodeConnected(boolean state,String port, String udid);
    }

    @Override
    public void run() {
        startNode();
    }

    public void executeCommandInWindows(String aCommand ) {
        File currDir = new File(System.getProperty("user.dir"));
        String line;
        try {
            ProcessBuilder probuilder = new ProcessBuilder("CMD", "/C", aCommand);
            probuilder.directory(currDir);
            Process process = probuilder.start();

            BufferedReader inputStream
                    = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errorStream
                    = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            // reading output of the command
            int inputLine = 0;
            while ((line = inputStream.readLine()) != null) {
                if (inputLine == 0) {
//                    System.out.printf("Output of the running command is: \n");
                }

//                System.out.println(line);
                inputLine++;
                if(inputLine ==4){
                    if (mListener!=null) {
                        mListener.onSeleniumNodeConnected(true,mPort,mUdid);
                    }
                }
            }
            // reading errors from the command
            int errLine = 0;
            while ((line = errorStream.readLine()) != null) {
                if (errLine == 0) {
                    System.out.println("Error of the command is: \n");

                }
                System.out.println(line);
                errLine++;
            }


        } catch (IOException e) {
            System.err.println("Exception occured: \n");
            System.err.println(e.getMessage());
            if (mListener!=null) {
                mListener.onSeleniumNodeConnected(false,mPort,mUdid);
            }
        }
    }
    public void startNode(){
        //appium --nodeconfig json/android-6.json -p 4723 -bp 5523 -U emulator-5554
//        executeCommandInWindows("appium --nodeconfig json/android-6.json -p "+mPort+" -bp 5523 -U "+mUdid);
        executeCommandInWindows("appium --local-timezone --session-override --full-reset -p "+mPort+" --address 127.0.0.1 --udid "+mUdid);
    }
}
