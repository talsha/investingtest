package controller;

import java.io.*;

/**
 * Created by devqa on 15/02/2017.
 */
public class AppiumServer{
    private String appiumWinInstallationDir = "C:\\Users\\tals\\AppData\\Roaming\\npm";
    private String appiumWinNode = "C:\\Program Files\\nodejs\\node.exe";
    private String appiumWinNodeModule = appiumWinInstallationDir + File.separator + "node_modules"
            + File.separator + "appium" + File.separator + "bin" + File.separator + "Appium.js";
//    private String appiumServicePort= "4725";
    private String appiumMacNode = "/usr/local/bin/node";
    private String appiumMacNodeModule = "/usr/local/lib/node_modules/appium/build/lib/main.js";
    private boolean started = false;
    private AppiumLogWriter appiumLogWriter = new AppiumLogWriter();
    private boolean stopped = false;


    private AppiumServerListener appiumServerListener;
    private String appiumServerport;
    private String deviceName;
    private String ip;
    private String bp;

    public AppiumServer(AppiumServerListener listener, String appiumServerport, String ip, String bp) {
        this.appiumServerListener= listener;
        this.appiumServerport = appiumServerport;
        this.deviceName= deviceName;
        this.ip = ip;
        this.bp = bp;
    }


    /**
     * Executes any command for Windows using ProcessBuilder of Java You can
     * change the first input parameter of ProcessBuilder constructor if your OS
     * is not windows operating system
     *
     * @param aCommand
     */
    public void executeCommandInWindows(String aCommand) {
        File currDir = new File(System.getProperty("user.dir"));
        String line;
        try {
            ProcessBuilder probuilder = new ProcessBuilder("CMD", "/C", aCommand);
            probuilder.directory(currDir);
            Process process = probuilder.start();

            BufferedReader inputStream
                    = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errorStream
                    = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            // reading output of the command
            int inputLine = 0;
            appiumLogWriter.emptyFile();
            while ((line = inputStream.readLine()) != null) {
                if (inputLine == 0) {
//                    System.out.printf("Output of the running command is: \n");
                    this.started = true;
                }

                appiumLogWriter.writeLineToFile(line);
//                System.out.println(line);
                inputLine++;
                if(inputLine ==4){
                    if (appiumServerListener!=null) {
//                    appiumServerListener.executeCommand(true,appiumServerport,appiumServerport);
//                        appiumServerListener.startAndroidTesting(deviceName,appiumServerport);
                }
            }
            }
            // reading errors from the command
            int errLine = 0;
            while ((line = errorStream.readLine()) != null) {
                if (errLine == 0) {
                    System.out.println("Error of the command is: \n");

                }
                System.out.println(line);
                errLine++;
//                stopAppiumServer();
//                startAppiumServer();
            }


        } catch (IOException e) {
            System.err.println("Exception occured: \n");
            System.err.println(e.getMessage());
            if (appiumServerListener!=null) {
//                appiumServerListener.executeCommand(false,appiumServerport,appiumServerport);
//                appiumServerListener.stopServer();
            }
        }
    }

    public void executeCommandInMac(String aCommand) {
        String line;
        try {
            Process p = Runtime.getRuntime().exec(aCommand);

            BufferedReader inputStream
                    = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader errorStream
                    = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // reading output of the command
            int inputLine = 0;
            appiumLogWriter.emptyFile();
            while ((line = inputStream.readLine()) != null) {
                if (inputLine == 0) {
//                    System.out.printf("Output of the running command is: \n");
                    this.started = true;
                }

                appiumLogWriter.writeLineToFile(line);
//                System.out.println(line);
                inputLine++;
                if(inputLine ==11){
                    if (appiumServerListener!=null) {
//                        appiumServerListener.startIOSTesting();
                    }
                }
            }

            // reading errors from the command
            int errLine = 0;
            while ((line = errorStream.readLine()) != null) {
                if (errLine == 0) {
                    System.out.println("Error of the command is: \n");
                }
                System.out.println(line);
                errLine++;
            }

        } catch (IOException e) {
            System.err.println("Exception occured: \n");
            System.err.println(e.getMessage());
//            if (appiumServerListener!=null) {
//                appiumServerListener.stopServer();
//            }
        }
    }


    /**
     * Starts appium server
     */
    public void startAppiumServer() {
//        stopped = false;
//        String os = System.getProperty("os.name");
//        if(os.contains("Windows")){
//            executeCommandInWindows("\"" + appiumWinNode + "\" \"" + appiumWinNodeModule
//                    + "\" " + "--local-timezone --session-override -p " + appiumServerport);
//            executeCommandInWindows(/*"\"" +appiumWinNode + "\*/"\""+ appiumWinNodeModule + "\" " + " --local-timezone --session-override -p " + appiumServerport);
//            executeCommandInWindows("appium --local-timezone --session-override --full-reset -p "+appiumServerport+" --address 127.0.0.1 --udid 034dd2d7215caaba");
//        }
//        else if(os.contains("Mac")){
//            executeCommandInMac("appium --local-timezone --session-override --full-reset -p 4723 --address 127.0.0.1");
//        }
    }

    /**
     * Stops appium server
     */
    public void stopAppiumServer() {
        stopped = true;
        String os = System.getProperty("os.name");
        if(os.contains("Windows")){
            executeCommandInWindows("cmd /c echo off & FOR /F \"usebackq tokens=5\" %a in"
                    + " (`netstat -nao ^| findstr /R /C:\""
                    + appiumServerport + "\"`) do (FOR /F \"usebackq\" %b in"
                    + " (`TASKLIST /FI \"PID eq %a\" ^| findstr /I node.exe`) do taskkill /F /PID %a)");
            System.out.println("Appium stopped");
        }
        else if(os.contains("Mac")){
            try {
                Runtime.getRuntime().exec("killall node");
                System.out.println("Appium stopped");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.setOut(System.out);
    }



    public void startNode(){
        //appium --nodeconfig json/android-6.json -p 4723 -bp 5523 -U emulator-5554
        executeCommandInWindows("appium --nodeconfig json/android-6.json -p "+appiumServerport+" -bp 5523 -U "+deviceName);
    }

    public void startServer(String ip, String port, String bp, String udid, String systemPort) {
        System.out.println("start appium");
        System.setOut(System.out);
        Runtime runtime = Runtime.getRuntime();
        try {
//            runtime.exec("call start cmd.exe /k PATH_TO_NODE_JS PATH_TO_APPIUM_JS --address IP_ADDRESS --port PORT_NUMBER\n")
            runtime.exec("cmd.exe /c start cmd.exe /k \"appium -a " + ip + " -p " + port + " -bp " + bp + " --session-override -dc \"{\"\"noReset\"\": \"\"false\"\"}\"\"");
//            Thread.sleep(10000);
//            appiumServerListener.executeCommand(false, ip, port, udid, systemPort);
        } catch (IOException e /*| InterruptedException e*/) {
            e.printStackTrace();
        }
    }

//cmd.exe /c
}
